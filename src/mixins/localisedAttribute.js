export default {
  methods: {
    localisedAttribute: function (obj, attr, lang) {
      return obj.attributes[attr][lang] || obj.attributes[attr]["enGb"];
    },
  },
};
