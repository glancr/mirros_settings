export default {
  methods: {
    go: function (route) {
      this.$store.dispatch("changeRoute", route);
    },
  },
};
