export default {
  methods: {
    emitFormUpdate: function (e) {
      this.$emit("formupdate", e);
    },
  },
};
