RadioBoxedLabel example:

```vue
<RadioBoxedLabel
  :labelleft="{
    for: 'network_connectiontype_option_wlan',
    text: 'WiFi'
  }"
  :labelright="{
    for: 'network_connectiontype_option_lan',
    text: 'Wired'
  }"
  :hideinput="false"
  :selected="'left'"
>
    <template v-slot:input-left>
    <input
        type="radio"
        name="network_connectiontype"
        id="network_connectiontype_option_wlan"
        value="wlan"
        :checked="true"
        @change="msg => msg"
        required
    />
    </template>

    <template v-slot:input-right>
    <input
        type="radio"
        name="network_connectiontype"
        id="network_connectiontype_option_lan"
        value="lan"
        :checked="false"
        @change="msg => msg"
        required
    />
    </template>
</RadioBoxedLabel>
```
