Basic TextInputFloatingLabel example:

```vue
<TextInputFloatingLabel for="network_ssid" text="Name of your WiFi">
            <input
              type="text"
              id="network_ssid"
              name="network_ssid"
              required
              value="My WiFi"
              placeholder="Your WiFi name"
              autocomplete="off"
            />
          </TextInputFloatingLabel>
```

Example with hint message:

```vue
<TextInputFloatingLabel
  for="given_name"
  text="Your given name"
  hint="We'll use this to address you in email notifications."
>
            <input
              type="text"
              id="given_name"
              name="given_name"
              required
              value=""
              placeholder="How would you like to be called?"
              autocomplete="off"
            />
          </TextInputFloatingLabel>
```

The hint may contain HTML:

```vue
<TextInputFloatingLabel
  for="api_key"
  text="API key"
  hint="<a href='https://glancr.de'>More details on our website</a>"
>
            <input
              type="text"
              id="api_key"
              name="api_key"
              required
              value=""
              placeholder="API key of your your coolApp integration"
              autocomplete="off"
            />
          </TextInputFloatingLabel>
```
