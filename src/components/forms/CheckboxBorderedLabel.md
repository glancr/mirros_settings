CheckboxBorderedLabel example:

```vue
<CheckboxBorderedLabel for="myConfigurationVariable" text="My label">
      <input
        type="checkbox"
        required
        aria-required="true"
        name="myConfigurationVariable"
        id="myConfigurationVariable"
        :value="true"
        :checked="true"
      />
</CheckboxBorderedLabel>
```
