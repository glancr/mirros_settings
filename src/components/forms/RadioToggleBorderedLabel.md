RadioToggleBorderedLabel example:

```vue
<RadioToggleBorderedLabel
  :active="false"
  fieldId="myConfigurationVariable"
  text="my label"
  @change="msg => msg"
/>
```

The `handleChange` method would be called inside your template component, i.e. it has to be defined inside your component's `methods` option.
