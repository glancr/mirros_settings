import { shallowMount } from "@vue/test-utils";
import AnimatedLoader from "@/components/AnimatedLoader.vue";

describe("AnimatedLoader.vue", () => {
  it("has svg", () => {
    const wrapper = shallowMount(AnimatedLoader);
    expect(wrapper.find("svg").exists()).toBe(true);
  });

  it("matches snapshot", () => {
    const wrapper = shallowMount(AnimatedLoader);
    expect(wrapper.element).toMatchSnapshot();
  });
});
