import { shallowMount, createLocalVue } from "@vue/test-utils";
import PrivacyConsentBlock from "@/components/config/PrivacyConsentBlock.vue";
import VueTranslate from "vue-translate-plugin";

describe("PrivacyConsentBlock.vue", () => {
  it("should emit a `formupdate` event on change", () => {
    const localVue = createLocalVue();
    localVue.use(VueTranslate);

    const wrapper = shallowMount(PrivacyConsentBlock, {
      propsData: {
        personal_privacyconsent: { value: "no" },
      },
      localVue,
    });
    wrapper.find("#personal_privacyconsent").setChecked();
    expect(wrapper.emitted().formupdate).toBeTruthy();
  });
});
