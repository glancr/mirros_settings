process.env.VUE_APP_VERSION = require("./package.json").version;

module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "/settings/" : "/",
  lintOnSave: true,
  runtimeCompiler: true,
  productionSourceMap: false,
  parallel: true,
  css: {
    sourceMap: true,
    // https://cli.vuejs.org/guide/css.html#passing-options-to-pre-processor-loaders
    loaderOptions: {
      scss: {
        additionalData: `@import "~@/assets/sass/settings";`,
      },
    },
  },
  configureWebpack: {
    devtool: "source-map",
    devServer: {
      allowedHosts: ["profibuch.local"],
    },
  },
};
